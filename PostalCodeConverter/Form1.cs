﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PostalCodeConverter
{
    public partial class Form1 : Form
    {
        public ListViewItem[] lvica;
        public string[] oca;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox2.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (Directory.Exists(textBox1.Text))
            {
                FBD1.SelectedPath = textBox1.Text;
            }

            if (FBD1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox1.Text = FBD1.SelectedPath;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DirectoryInfo Di;
            FileInfo[] FList;
            string[] tmp;
            listView1.Items.Clear();
            if (Directory.Exists(textBox1.Text))
            {
                Di = new DirectoryInfo(textBox1.Text);

                FList = Di.GetFiles("*.txt");

                foreach (FileInfo fi in FList)
                {
                    tmp = new string[2];
                    tmp[0] = fi.Name;

                    if (comboBox1.Items.IndexOf(fi.Name.Replace(".txt","")) <= -1)
                    {
                        tmp[1] = comboBox1.Items[0].ToString();
                    }
                    else
                    {
                        tmp[1] = comboBox1.Items[comboBox1.Items.IndexOf(fi.Name.Replace(".txt",""))].ToString();
                    }


                    listView1.Items.Add(new ListViewItem(tmp));
                }
            }

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                textBox2.Text = listView1.SelectedItems[0].SubItems[0].Text;

                comboBox1.SelectedItem = (object)listView1.SelectedItems[0].SubItems[1].Text;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count > 0)
            {
                listView1.SelectedItems[0].SubItems[1].Text = comboBox1.SelectedItem.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ThreadStart ThrdS = new ThreadStart(ConvertWork);
            Thread Thrd = new Thread(ThrdS);

            enableContols(false);

            lvica = new ListViewItem[listView1.Items.Count];
            listView1.Items.CopyTo(lvica, 0);
            oca = comboBox1.Items.Cast<string>().ToArray<string>();

            progressBar1.Maximum = listView1.Items.Count;

            Thrd.Start();
        }

        public void enableContols(bool Enable)
        {
            textBox1.Enabled = Enable;
            listView1.Enabled = Enable;
            comboBox1.Enabled = Enable;
            comboBox2.Enabled = Enable;
            checkBox2.Enabled = Enable;

            button1.Enabled = Enable;
            button2.Enabled = Enable;

        }

        public void ConvertWork()
        {
            StreamReader Sr;
            StreamWriter Sw;

            int cnt = 0;
            string[] Line;
            bool FirstLine = false;
            string rg, dir;

            dir = textBox1.Text;
            dir += (textBox1.Text.EndsWith("\\") ? "" : "\\");

            for (int i = 0; i < lvica.Length; i++)
            {
                if (lvica[i].SubItems[1].Text == oca[0])
                {
                    cnt++;
                    this.Invoke((MethodInvoker)delegate
                    {
                        progressBar1.Value = cnt;
                    });
                    continue;
                }

                rg = PCUtils.RegionCodes[Array.IndexOf<string>(oca, lvica[i].SubItems[1].Text)];

                Sr = new StreamReader(dir + lvica[i].SubItems[0].Text, Encoding.GetEncoding("euc-kr"));
                Sw = new StreamWriter(dir + "postal_code_" + rg + ".sql", false, Encoding.UTF8);

                FirstLine = false;

                while (!Sr.EndOfStream)
                {
                    Line = Sr.ReadLine().Split('|');

                    if (!FirstLine) { FirstLine = true; continue; }

                    if(Line.Length < 24) {
                        continue;
                    }
                    
                    Sw.Write("insert into postal_code" + (checkBox2.Checked ? "_" + rg : "") + " (postal_code, province, city, district, road, num1, num2, dong, lot1, lot2, old_postal_code) values (");
                    Sw.Write("\"" + Line[0] + "\",");
                    Sw.Write("\"" + Line[1] + "\",");
                    Sw.Write("\"" + Line[3] + "\",");
                    Sw.Write("\"" + Line[5] + "\",");
                    Sw.Write("\"" + Line[8] + "\",");
                    Sw.Write("\"" + (Line[11].Length != 0 ? Line[11] : "0") + "\",");
                    Sw.Write("\"" + (Line[12].Length != 0 ? Line[12] : "0") + "\",");
                    Sw.Write("\"" + Line[19] + "\",");
                    Sw.Write("\"" + (Line[21].Length != 0 ? Line[21] : "0") + "\",");
                    Sw.Write("\"" + (Line[23].Length != 0 ? Line[23] : "0") + "\",");
                    Sw.Write("\"\");\r\n");
                }

                Sr.Close();
                Sw.Close();

                cnt++;
                this.Invoke((MethodInvoker)delegate
                {
                    progressBar1.Value = cnt;
                });
            }

            this.Invoke((MethodInvoker)delegate
            {
                enableContols(true);
                MessageBox.Show("완료하였습니다!");
            });
            
            /*
             * this.Invoke((MethodInvoker)delegate {
    someLabel.Text = newText; // runs on UI thread
});
             * */
        }
    }
}
